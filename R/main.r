#' @import bnutil
#' @import shiny
#' @import dplyr
#' @import data.table
#' @export


#' @export
shinyServerRun = function(input, output, session, context) {

  output$body = renderUI({

    sidebarLayout(

      sidebarPanel(
                     helpText("LOESS regression"),
                     selectInput("whatref", "Use as reference:", c()),
                     sliderInput("span", "LOESS span",min = 0, max = 1, step = 0.01, value = 0.7),
                     actionButton("start", "Run"),
                     verbatimTextOutput("status")),
      mainPanel(
        plotOutput("pres")
      )
    )
  })



  getRunFolderReactive = context$getRunFolder()
  getStepFolderReactive = context$getFolder()
  getDataReactive = context$getData()



  observe({


    getData=getDataReactive$value
    if (is.null(getData)) return()

    getRunFolder = getRunFolderReactive$value
    if(is.null(getRunFolder)) return()

    getStepFolder = getStepFolderReactive$value
    if(is.null(getStepFolder)) return()

    bndata = getData()
    df = bndata$data

    if(bndata$hasXAxis){
      updateSelectInput(session, "whatref", choices = "X-Axis")
      if(length(levels(factor(df$QuantitationType))) > 1){
        stop("No more than 1 qunatitation type allowed with X-Axis as reference.")
      }

    } else {
      qt = levels(factor(df$QuantitationType))
      if (length(qt) != 2){
        stop("No X-axis, need exactly 2 quantitation types")
      }
      updateSelectInput(session, "whatref", choices = qt)
    }


    output$status = renderText({

      if(input$start >0){
        showNotification(ui = "Running LOESS ...", type = "message", closeButton = FALSE, duration = NULL)
        if(!bndata$hasXAxis){
          dfx = df %>% filter(QuantitationType == input$whatref) %>% select(rowSeq, colSeq, value)
          dfy = df %>% filter(QuantitationType != input$whatref) %>% select(rowSeq, colSeq, value)
          dfin = left_join(dfx, dfy, by = c("rowSeq", "colSeq"))
        } else {
          dfin = df%>%mutate(value.y = value, value.x = df[[bndata$xAxisColumnName]]) %>% select(rowSeq, colSeq, value.x, value.y)
        }
        result = dfin %>% group_by(colSeq) %>% do(loessOperator(., span = input$span))
        meta.result = data.frame(labelDescription = c("rowSeq", "colSeq", "yFit", "yRes"),
                              groupingType = c("rowSeq", "colSeq", "QuantitationType", "QuantitationType"))
        aResult = AnnotatedData$new(data = result, metadata = meta.result)
        context$setResult(aResult)
        return("Done")
      }
    })
  })
}

#' @export
shinyServerShowResults = function(input, output, session, context){

}
